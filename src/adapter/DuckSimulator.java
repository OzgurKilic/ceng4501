package adapter;


import java.util.List;
import java.util.ArrayList;


public class DuckSimulator {

	List<Duck> ducks= new ArrayList<>();
	
	void addDuck(Duck duck){
		ducks.add(duck);
	}
	
	void flyDucks(){
		for(Duck duck: ducks){
			duck.fly();
		}
	}
	
	
	public static void main(String[] args){
		DuckSimulator sim = new DuckSimulator();
		
		sim.addDuck(new MallardDuck());
		sim.addDuck(new TurkeyAdapter(new WildTurkey()));
		
		sim.flyDucks();
	}
}
