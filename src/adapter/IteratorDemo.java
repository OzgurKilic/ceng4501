package adapter;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;


public class IteratorDemo {

	List lst = new ArrayList();
	
	Vector vec = new Vector();
	
	public static void main(String[] args) {
		IteratorDemo demo = new IteratorDemo();
		demo.lst.add("A");
		demo.lst.add("B");
		
		Iterator itr = demo.lst.iterator();
		while(itr.hasNext()){
			Object obj = itr.next();
			System.out.println(obj);
		}
		
		demo.vec.addElement("A");
		demo.vec.addElement("B");
		
		Enumeration e =  demo.vec.elements();
		while (e.hasMoreElements()){
			System.out.println(e.nextElement());
		}
		
	}

}
