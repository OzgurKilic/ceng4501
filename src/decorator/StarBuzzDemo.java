package decorator;

import java.util.ArrayList;
import java.util.List;

public class StarBuzzDemo {

	public static void main(String[] args) {
		
		List<Beverage> items = new ArrayList<>();
		Beverage decaf = new Decaf();
		decaf = new Milk(decaf); 
		decaf = new Creme(decaf);
		items.add(decaf);
		

		Beverage darkRoast = new DarkRoast();
		darkRoast = new Mocha(new Mocha(darkRoast));
		items.add(darkRoast);
		
		Beverage starKing= new StarKing();
		starKing = new Creme(starKing);
		items.add(starKing);
		
		for(Beverage bev: items){
			System.out.println("cost of " + bev.getDescription() + 
					" is " + bev.cost());			
		}
		
		
	}

}
