package decorator;

public class HouseBlend extends Beverage {

	public HouseBlend() {
		description = "House Blend";
	}
	
	@Override
	public float cost() {
		return  (float) 3.0;
	}

}
