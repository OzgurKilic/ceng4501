package decorator;

public class Whip extends CondimentDecorator {

	public Whip(Beverage decoratedBeverage) {
		super(decoratedBeverage);
	}
	
	@Override
	public float cost() {
		return (float) 0.4 + decoratedBeverage.cost();
	}

	@Override
	public String getDescription() {
		
		return decoratedBeverage.getDescription() + " Whip";
	}

}
