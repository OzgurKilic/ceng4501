package decorator;

public class Mocha extends CondimentDecorator {

	
	
	public Mocha(Beverage decoratedBeverage) {
		super(decoratedBeverage);
	}
	
	@Override
	public float cost() {
		return (float) 0.5 + decoratedBeverage.cost();
	}

	@Override
	public String getDescription() {
		
		return decoratedBeverage.getDescription() + " Mocha";
	}

}
