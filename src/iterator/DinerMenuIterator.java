package iterator;

public class DinerMenuIterator implements java.util.Iterator  {

	MenuItem[] items;
	
	int position = 0;
	
	public DinerMenuIterator(MenuItem[] items) {
		this.items = items;
	}
	
	@Override
	public boolean hasNext() {
		if (items[position] == null || position>=items.length){
			return false;
		}
		return true;
	}

	@Override
	public Object next() {
		Object item = items[position];
		position++;
		return item;
	}

}
