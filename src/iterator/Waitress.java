package iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Waitress {
	
	List<Menu> menus;
	Collection a;
	
	public Waitress(List<Menu> menus) {
		super();
		this.menus = menus;
	}

	
	public void printMenu(){
		System.out.println("MENU");
		for (Menu menu: menus){
			System.out.println(menu.getMenuDefinition());
			printMenu(menu.createIterator());
		}
	}
	
	void printMenu(java.util.Iterator  iterator){
		while(iterator.hasNext()){
			MenuItem menuItem = (MenuItem)iterator.next();
			System.out.print(menuItem.getName() + " ");
			System.out.println(menuItem.getPrice() + " ");
			System.out.println(menuItem.getDescription());
		}
	}

}
