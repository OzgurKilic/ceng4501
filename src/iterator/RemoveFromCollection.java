package iterator;

import java.util.ArrayList;
import java.util.List;

public class RemoveFromCollection {

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<>();

		numbers.add(5);
		numbers.add(7);
		numbers.add(8);
		numbers.add(4);
		numbers.add(9);
		numbers.add(11);
		System.out.println("BEFORE");
		for(int i = 0; i< numbers.size(); i++){
			System.out.print(numbers.get(i) +" ");
		} 
		
		java.util.Iterator<Integer> itr = numbers.iterator();
		while(itr.hasNext()){
			if (itr.next() % 2 == 1){
				itr.remove();
			}
		}
	
		
		System.out.println();
		System.out.println("AFTER");
		for(int i = 0; i< numbers.size(); i++){
			System.out.print(numbers.get(i) + " ");
		}
		
		
	}

}
