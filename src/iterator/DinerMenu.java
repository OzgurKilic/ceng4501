package iterator;

public class DinerMenu implements Menu, java.util.Iterator{

	static final int MAX_ITEMS = 6;

	private MenuItem[] menuItems;

	int numberOfItems;

	int position;
	public DinerMenu() {
		menuItems = new MenuItem[MAX_ITEMS];
		addItem("Diner 1", " Content Diner 1", true, 3.99);
		addItem("Diner 2", " Content Diner 2", true, 4.99);
		addItem("Diner 3", " Content Diner 3", true, 6.99);

	}

	public void addItem(String name, String description, boolean vegeterian, double price) {
		if (numberOfItems >= MAX_ITEMS) {
			System.out.println("Can not Add a new Item");
		} else {
			MenuItem menuItem = new MenuItem(name, description, vegeterian, price);
			menuItems[numberOfItems] = menuItem;
			numberOfItems++;
		}

	}

	public java.util.Iterator  createIterator() {
		return this;
	}

	@Override
	public String getMenuDefinition() {
		// TODO Auto-generated method stub
		return "-------DINER--------";
	}

	@Override
	public boolean hasNext() {
		if (menuItems[position] == null || position>=menuItems.length){
			return false;
		}
		return true;
	}

	@Override
	public Object next() {
		Object item = menuItems[position];
		position++;
		return item;
	}	
}
