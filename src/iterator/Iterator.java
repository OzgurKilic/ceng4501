package iterator;

public interface Iterator {
	boolean hasNext();
	default Object next(){
		return null;
	}
}
