package iterator;

import java.util.ArrayList;

public class PancakeHouseMenuIterator implements java.util.Iterator  {

	ArrayList menuItems;
	int position;
	
	public PancakeHouseMenuIterator(ArrayList menuItems) {
		this.menuItems = menuItems;
	}
	
	
	@Override
	public boolean hasNext() {
		if(position >= menuItems.size()){
			return false;
		}
		return true;
	}

	@Override
	public Object next() {
		Object menuItem = menuItems.get(position);
		position++;
		return menuItem;
	}

}
