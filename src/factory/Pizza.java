package factory;

public abstract class Pizza {
	Dough dough;
	Sauce sauce;
	Cheese cheese;
	String description;
		
	public Pizza(PizzaIngredientFactory ingredientFactory) {
		dough = ingredientFactory.createDough();
		sauce = ingredientFactory.createSauce();
		cheese = ingredientFactory.createCheese();
		
	}
	
	public void prepare(){
		System.out.println("Preparing " + description);
	}
	
	public void bake(){
		System.out.println("Baking " + description);
		
	}
	
	public void cut(){
		System.out.println("Cutting " + description);
		
	}

	public void box(){
		System.out.println("Boxing " + description);
		
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

}
