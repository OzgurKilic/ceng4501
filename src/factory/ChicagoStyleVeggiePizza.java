package factory;

public class ChicagoStyleVeggiePizza extends Pizza {

	public ChicagoStyleVeggiePizza(PizzaIngredientFactory factory) {
		super(factory);
	}
}
