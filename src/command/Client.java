package command;

public class Client {

	public static void main(String[] args) {
		SimpleRemoteControl smr = new SimpleRemoteControl();
		smr.setCommand(new LightOnCommand(new Light()));

		smr.buttonWasPressed();
	}

}
