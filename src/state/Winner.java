package state;

public class Winner implements State {

	GumballMachine machine;
	
	public Winner(GumballMachine machine) {
		this.machine = machine;
	}
	
	@Override
	public void insertQuarter() {
		System.out.println("Wait will give 2 gumballs");

	}

	@Override
	public void ejectQuarter() {
		System.out.println("It's too late, but you are the winner");

	}

	@Override
	public void turnCrank() {
		System.out.println("Not applicable");

	}

	@Override
	public void dispense() {
		System.out.println("You are the winner");
		machine.releaseBall();
		if(machine.getGbCount() == 0){
			machine.setState(machine.sold_out);
		}else{
			machine.releaseBall();
			if(machine.getGbCount() == 0){
				machine.setState(machine.sold_out);
			}else{
				machine.setState(machine.no_quarter);
			}
		}

	}

}
