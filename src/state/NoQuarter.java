package state;

public class NoQuarter implements State {

	GumballMachine machine;
	
	public NoQuarter(GumballMachine machine) {
		this.machine = machine;
	}
	
	@Override
	public void insertQuarter() {
		machine.setState(machine.has_quarter);
		System.out.println("You have inserted a quarter!");

	}

	@Override
	public void ejectQuarter() {
		System.out.println("You haven't inserted a quarter!");
	}

	@Override
	public void turnCrank() {
		System.out.println("You haven't inserted a quarter!");

	}

	@Override
	public void dispense() {
		System.out.println("You have to pay first!");
	}

}
