package singleton;

public class AnotherClass {

	public void doSomething(){
		MyClass my = MyClass.getInstance(5);
		MyClass my2 = MyClass.getInstance(4);
		
		System.out.println(my.num);
		
		System.out.println(my2.num);
	}
	
	public static void main(String[] args) {
		AnotherClass a = new AnotherClass();
		a.doSomething();

	}

}
