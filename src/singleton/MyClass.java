package singleton;

public class MyClass {

	int num;

	private static MyClass instance;

	private MyClass(int num) {
		this.num = num;
	}

	public static MyClass getInstance(int num) {
		if (instance == null) {

			synchronized (MyClass.class) {
				if (instance == null) {
					instance = new MyClass(num);
				}
			}


		}
		return instance;
	}
}
