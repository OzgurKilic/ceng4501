package templatemethod;

public abstract class CafeinBeverage {

	public final void prepareRecipe(){
		boilWater();
		brew();
		pourInCup();
		addCondiments();
		
	}
	
	protected abstract void brew(); 
	
	protected abstract void addCondiments();
	
	protected void pourInCup() {
		System.out.println("Pouring in the cup");

	}

	protected final void boilWater() {
		System.out.println("Boiling Water");
	}
}
