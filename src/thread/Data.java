package thread;

public class Data {

	int num;
	
	public synchronized void increment(){
		num++;
	}
	
	public static void main(String[] args) throws Exception{
		Data data = new Data();
		ThreadA a = new ThreadA(data);
		ThreadB b = new ThreadB(data);
		
		a.start();
		b.start();
		
		a.join();
		b.join();
		System.out.println(data.num);
	}
}
